pub type MethodResult<S> = Result<S, S>;
