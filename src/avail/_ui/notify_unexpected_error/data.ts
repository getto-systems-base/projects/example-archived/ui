import { RemoteCommonError } from "../../../../ui/vendor/getto-application/infra/remote/data"

export type NotifyUnexpectedErrorRemoteError = RemoteCommonError
